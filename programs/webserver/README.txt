FALSYS WEBSERVER
~~~~~~~~~~~~~~~~

This is a webserver written in FALSys.
To use it, move your static files into the root of this folder then run

    $ falsys webserver.fsys

or

    $ falsys webserver.small.fsys
