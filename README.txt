 _____ _    _     ____            
|  ___/ \  | |   / ___| _   _ ___ 
| |_ / _ \ | |   \___ \| | | / __| FALSE Implementation with syscalls
|  _/ ___ \| |___ ___) | |_| \__ \ by Matthilde "h34ting4ppliance"
|_|/_/   \_\_____|____/ \__, |___/ Licensed under MIT License
                        |___/

TABLE OF CONTENTS
 - Introduction
 - Important Note
 - Debugger extensions
 - Why is the lambda a buffer?
 - Building
 - Usage
 - Licensing

INTRODUCTION
~~~~~~~~~~~~

FALSys is an implementation of the FALSE esoteric programming language with a
little twist: The inline assembly instruction has been turned into a syscall
instruction.
Since this implementation doesn't support inline assembly (I didn't wanted to),
I have given the ` another use which is the ability to make Linux syscalls.

This implementation also has a specific way to store lambdas: It malloc.
For now, lambdas are not freed until you quit the program but some
optimizations will be added if I am not lazy.
This way you can then also use lambdas as buffer.

For example, writing "Hello, World!" in FALSE goes like this:
    
    "Hello, World!
    "

However, you can also do this using syscalls by using a lambda as a buffer like
this:

    14              { rdx, Length of the string }
    [Hello, World!
    ]               { rsi, Buffer }
    1               { rdi, File Descriptor }
    1               { rax, Syscall }
    `               { do the magic }

This example shows at the same time how a lambda can be used as a buffer and
also the ability to perform syscalls.

There are other examples in the "programs" folder.

IMPORTANT NOTE
~~~~~~~~~~~~~~

This implementation of FALSE may not work on some original programs written for
the original compiler. In fact some programs could use the inline assembly
which is not implemented in the version I wrote (used for syscalls instead.)
And if I added inline assembly, it also wouldn't work anyways for the simple
reason that the Amiga has the Motorola 68000.

So unless you managed to compile this for the Amiga or you managed to port
the inline assembly for your arch. (assuming there had actually inline
assembly), it won't work.

WHY IS THE LAMBDA A BUFFER?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Well, at the first place it was designed so I could make an interpreter without
having to worry about lambdas being lost on the next input. I considered it as
a flaw at first then found out there could have some (hacky) good uses like
using it as a way to store data other than a .. z (It also makes it kinda
turing complete. Pog)

So, a good mistake I'd say?

DEBUGGER EXTENSIONS
~~~~~~~~~~~~~~~~~~~

There are a few debugger extensions I have added. 'D' is the prefix followed
by another character.

    Ds      will dump the whole stack.
    Dl      will pop a lambda from the stack and output it's content.

BUILDING
~~~~~~~~

Build it using make. If you want it in your bin folder, move it yourself.
You don't have make? Type this:

    gcc false.c -std=c99 -o falsys -Wall -Wextra

It does the same thing.

USAGE
~~~~~

    falsys FILENAME     Runs a file
    falsys -v           Display the version

LICENSING
~~~~~~~~~

The implementation itself is licensed under MIT License.
However, I do not own the FALSE programming language and I haven't contributed
in it.
The FALSE programming language has been created by Wouter van Oortmerseen who
has a pretty good site, you can check it out there: http://strlen.com
