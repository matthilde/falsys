/*
 * FALSys by Matthilde "h34ting4ppliance"
 *
 * false.c
 *
 * This is the implementation of the FALSE programming language with a few
 * twists. I won't repeat myself so please check README.txt in the repo.
 */

#define _GNU_SOURCE
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>

// FALSE Programming language implementation in C.
// http://strlen.com/files/lang/false/false.txt

#define STACK_SIZE 1024

typedef struct
{
    size_t variables[26];
    size_t stack[STACK_SIZE];
    size_t stackptr;
} false_t;

false_t new_false()
{
    false_t f;
    int i;

    for (i = 0; i < 26; i++)
        f.variables[i] = 0;
    f.stackptr = 0;

    return f;
}

// Stack functions
void fpush(false_t *f, size_t value)
{ f->stack[f->stackptr++] = value; }
size_t fpop(false_t *f)
{
    if (f->stackptr == 0)
    {
        puts("Stack underflow.");
        exit(1);
    }
    return f->stack[--f->stackptr];
}

void stacktrace(false_t *f)
{
    putchar('[');
    for (size_t i = 0; i < f->stackptr; ++i)
        printf("%ld ", f->stack[i]);
    puts("\b]");
}

// Pushing and popping lambda gives the user an abstraction layer
// to avoid dealing with memory allocation. The lambda functions will
// do it for them.

// Push lambda
void push_lambda(false_t *f, const char* program,
                size_t begin, size_t sz)
{
    char* l = (char*)malloc(sizeof(char) * (sz+1));
    size_t i;
    for (i = 0; i < sz; i++)
        l[i] = program[i+begin];
    l[i] = '\0';

    fpush(f, (size_t)l);
}
// Pop lambda
void pop_lambda(false_t *f)
{
    char* ptr = (char*)fpop(f);
    free(ptr);
}
// Execute lambda
void eval(false_t*, const char*, size_t);
void execute_lambda(false_t *f, size_t ptr)
{
    char* l = (char*)ptr;
    size_t sz = strlen(l);

    eval(f, l, sz);
}
// Parse lambda
size_t parse_lambda(false_t *f, const char* program, size_t begin)
{
    int count = 1;
    const char* tmp = program + begin;
    size_t sz = 0;

    while (count)
    {
        sz++;
        switch (*(++tmp))
        {
            case '[':
                count++;
                break;
            case ']':
                count--;
                break;
        }
    }

    if (--sz <= 0)
        return 0;
    push_lambda(f, program, begin+1, sz);
    return sz;
}

// Eval some code
void eval(false_t *f, const char* program, size_t sz)
{
    size_t intbuffer = -1;
    char c;
    size_t a, b, d, e;

    for (size_t i = 0; i < sz; ++i)
    {
        c = program[i];
        
        //#define _DEBUGMODE
        #ifdef _DEBUGMODE
        printf("'%c': ", c);
        stacktrace(f);
        #endif

        if (isdigit(c))
        {
            if ((ssize_t)intbuffer == -1)
                intbuffer = 0;
            intbuffer = intbuffer * 10 + (c - '0');
            continue;
        }
        else if ((ssize_t)intbuffer != -1)
        {
            fpush(f, intbuffer);
            intbuffer = -1;
        }
        
        if (c >= 'a' && c <= 'z')
        {
            fpush(f, (size_t)(&(f->variables[c - 'a'])));
            continue;
        }
        // Debug functions
        else if (c == 'D')
        {
            c = program[++i];
            switch (c)
            {
                // Display content of lambda
                case 'l':
                    printf("LAMBDA DUMP: %s\n", (char*)fpop(f));
                    break;
                case 's':
                    stacktrace(f);
                    break;
            }
            continue;
        }

        switch (c)
        {
            // Brackets
            case '{':
                while (program[++i] != '}');
                break;
            case '"':
                while (program[++i] != '"')
                    putchar(program[i]);
                break;
            case '[':
                i += parse_lambda(f, program, i) + 1;
                break;

            // Variable and pointer manipulation
            case ':':
                *((size_t*)fpop(f)) = fpop(f);
                break;
            case ';':
                fpush(f, *((size_t*)fpop(f)));
                break;
            case '!':
                execute_lambda(f, fpop(f));
                break;
            case '?':
                a = fpop(f);
                b = fpop(f);

                execute_lambda(f, b);
                if (fpop(f))
                    execute_lambda(f, a);
                break;
            case '#':
                a = fpop(f);
                b = fpop(f);
                
                // printf("%s\n%s\n---", (char*)a, (char*)b);
                execute_lambda(f, b);
                while (fpop(f))
                {
                    execute_lambda(f, a);
                    execute_lambda(f, b);
                }
                break;

            // Arithmetic
            case '+':
                fpush(f, fpop(f) + fpop(f));
                break;
            case '-':
                a = fpop(f);
                fpush(f, fpop(f) - a);
                break;
            case '*':
                fpush(f, fpop(f) * fpop(f));
                break;
            case '/':
                fpush(f, fpop(f) / a);
                break;
            case '_':
                fpush(f, 0 - fpop(f));
                break;

            // Boolean Logic
            case '=':
                fpush(f, fpop(f) == fpop(f));
                break;
            case '>':
                fpush(f, fpop(f) < fpop(f));
                break;
            case '&':
                fpush(f, fpop(f) & fpop(f));
                break;
            case '|':
                fpush(f, fpop(f) | fpop(f));
                break;
            case '~':
                fpush(f, !(fpop(f)));
                break;

            // Stack operations
            // dup
            case '$':
                a = fpop(f);
                fpush(f, a);
                fpush(f, a);
                break;
            // drop
            case '%':
                fpop(f);
                break;
            // swap
            case '\\':
                a = fpop(f);
                b = fpop(f);
                fpush(f, a);
                fpush(f, b);
                break;
            // rot
            case '@':
                a = fpop(f);
                b = fpop(f);
                d = fpop(f);
                fpush(f, b);
                fpush(f, a);
                fpush(f, d);
                break;
            // pick (ø) (ffs UTF-8)
            case (char)0xc2:
                ++i;
                if (program[i] == (char)0xb8)
                {
                    a = fpop(f);
                    fpush(f, f->stack[f->stackptr - a]);
                }
                break;
            
            // putc
            case ',':
                putchar(fpop(f));
                break;
            // printnum
            case '.':
                printf("%ld", (ssize_t)fpop(f));
                break;
            // getc
            case '^':
                fpush(f, getchar());
                break;
            // pushes char
            case '\'':
                fpush(f, program[++i]);
                break;
            // This implementation of FALSE does not support
            // the ` instruction (inline assembly). So instead,
            // this instruction will be used to perform syscalls
            case '`':
                a = fpop(f);    // rax
                b = fpop(f);    // rdi
                d = fpop(f);    // rsi
                e = fpop(f);    // rdx
                
                fpush(f, syscall(a, b, d, e));
                break;
            
            case ' ':
            case '\t':
            case '\r':
            case '\n':
            case '\0':
                break;

            default:
                printf("%ld '%c': Unknown instruction\n", i, c);
                exit(1);
                break;
        }
    }

    if (intbuffer != -1)
        fpush(f, intbuffer);
}

// Reads file and outputs a string
// Must be freed at the end of the program.
char* read_file(const char* filename)
{
    int err, c;

    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("false");
        exit(2);
    }
    err = fseek(f, 0, SEEK_END);
    if (err)
        exit(2);
    ssize_t sz = ftell(f);
    if (sz == -1)
        exit(2);

    char* rf = (char *)malloc(sz * sizeof(char));
    char* tmp = rf;

    rewind(f);
    while ((c=fgetc(f)) != EOF)
        *(tmp++) = c;

    return rf;
}

void usage(const char* exename)
{
    printf(
        "Usage: %s FILENAME\n"
        "       %s -v\n", exename, exename);
    exit(2);
}

void version()
{
    printf(
        "FALSys esoteric programming language, a FALSE extension with\n"
        "Linux syscalls by Matthilde 'h34ting4ppliance'\n"
        "FALSE programming language by Wouter van Oortmerssen\n\n"
        "Licensed under MIT License.\n");
    exit(0);
}

int main(int argc, char** argv)
{
    if (argc != 2)
        usage(argv[0]);
    else if (strcmp(argv[1], "-v") == 0)
        version();

    char* f = read_file(argv[1]);
    false_t fdata = new_false();

    eval(&fdata, f, strlen(f));
    
    free(f);
    return 0;
}
